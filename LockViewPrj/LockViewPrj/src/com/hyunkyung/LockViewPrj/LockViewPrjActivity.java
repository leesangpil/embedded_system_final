package com.hyunkyung.LockViewPrj;

import java.util.List;

import com.hyunkyung.LockViewPrj.LockPatternView.Cell;
import com.hyunkyung.LockViewPrj.LockPatternView.OnPatternListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class LockViewPrjActivity extends Activity {
	
	// 여기 밑에 한줄
	public static int buy_index = 8;  // 0
	
	////////////////여기 밑에 3줄
	static {
		System.loadLibrary("clcdjni");
	}
	///////////////여기 밑에 2중
	public native int ClcdCon(String clcdtx);
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        
        final Intent load_Success = this.getPackageManager().getLaunchIntentForPackage("net.hybus.success");
        
        // 사인해 주세요
        ClcdCon("PLEASE SIGN !!  YOUR PATTERN !!");
        
        OnPatternListener patternListener = new OnPatternListener() {
			
			@Override
			public void onPatternStart() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPatternDetected(List<Cell> pattern) {
				
				int real_index = 0;		// 내가 그린패턴과 설정된 패턴이 일치하는지 확인할수 있는 인덱스 변수
				
				for(int i=0; i<pattern.size(); i++)
				{
					Cell pCell = pattern.get(i);
					Log.d("qwer", "qwer  pCell.row : " + pCell.row  + "  pCell.column : " + pCell.column);
					//Log.d("qwer", "qwer  " + pCell.toString());
					
					
					
					switch(i)
					{
					case 0:
						if(pCell.row == 0 && pCell.column == 0)
						{
							real_index++;
							break;
						}
						
					case 1:
						if(pCell.row == 1 && pCell.column == 0)
						{
							real_index++;
							break;
						}
						
					case 2:
						if(pCell.row == 1 && pCell.column == 1)
						{
							real_index++;
							break;
						}
						
					case 3:
						if(pCell.row == 0 && pCell.column == 1)
						{
							real_index++;
							break;
						}
						
					}
				}
				
				Log.d("qwer", "qwer  " + real_index);
				
				if(real_index == 4)
				{
					if(buy_index == 0)
					{
						//////////여기 밑에 1줄
				        ClcdCon("BALANCE : 9000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 1)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 8000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 2)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 7000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 3)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 6000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 4)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 5000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 5)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 4000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 6)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 3000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 7)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 2000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 8)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 1000              WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 9)
					{
						//////////여기 밑에 1줄
						ClcdCon("BALANCE : 0                 WON");
				        buy_index++;
				        moveTaskToBack(true);
					    finish();
				        startActivity(load_Success);
					}
					else if(buy_index == 10)
					{
						//////////여기 밑에 1줄
				        ClcdCon("MONEY EMPTY");
				        moveTaskToBack(true);
					    finish();
				        //buy_index++;
					}
					else{}
					//여기 밑에 두줄은 종료하는 것
					//moveTaskToBack(true);
				    //finish();
				    // 패턴이 맞으면 다른 화면 실행
				    //startActivity(load_Success);
				}
				else
				{
					ClcdCon("PATTERN ERROR !!");
					real_index = 0;
				}
				try{
					Thread.sleep(2000);
				}catch(Exception e){}
				ClcdCon("");
			}
			
			@Override
			public void onPatternCleared() {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onPatternCellAdded(List<Cell> pattern) {
				// TODO Auto-generated method stub
				
			}
		};
		
		LockPatternView pPatternView = (LockPatternView)findViewById(R.id.patternview);
		pPatternView.setOnPatternListener(patternListener);
    }
}