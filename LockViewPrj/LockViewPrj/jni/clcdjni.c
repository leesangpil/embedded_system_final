#include <jni.h>
#include <fcntl.h>
#include <unistd.h>
#define CLCD_DEV "/dev/clcd"

int clcd_fd;

jint Java_com_hyunkyung_LockViewPrj_LockViewPrjActivity_ClcdCon(JNIEnv* env, jobject thiz,
		jstring clcdtx) {
	close(clcd_fd);
	const char *str;
	clcd_fd = open(CLCD_DEV, O_RDWR);
	str = (*env)->GetStringUTFChars(env, clcdtx, NULL);
	write(clcd_fd, str, 32);
	close(clcd_fd);
	return 0;
}
