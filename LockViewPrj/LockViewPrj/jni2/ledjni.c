#include <jni.h>
#include <fcntl.h>
#include <unistd.h>
#define LED_DEV "/dev/led"
int led_fd;
jint Java_com_hyunkyung_LockViewPrj_LockViewPrjActivity_LedCon(JNIEnv* env, jobject thiz, jint led){

	close(led_fd);
	led_fd = open(LED_DEV, O_RDWR);
	write(led_fd, &led, 1);
	return 0;
}
