package net.hybus.nfctagreader;


import android.app.Activity;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TagReaderActivity extends Activity {
	
	private static final String TAG = "TagReader";
	
	private TextView mText;
	private Button mButton1;
	private Button mButton2;
	
	static {
		System.loadLibrary("ledjni");
	}
	///////////////여기 밑에 1중
	public native int LedCon(int led);
	
	
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reader_main);
        
        mText = (TextView) findViewById(R.id.textView1);
        mButton1 = (Button) findViewById(R.id.button1);
        mButton2 = (Button) findViewById(R.id.button2);
        
        Intent intent = getIntent();
        Intent loadpattern = this.getPackageManager().getLaunchIntentForPackage("com.hyunkyung.LockViewPrj");
        final Intent load_bluetooth = this.getPackageManager().getLaunchIntentForPackage("com.example.android.BluetoothChat");
        
        String action = intent.getAction();
        
        mButton1.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        	
        		LedCon(0x00);
        		try{
        		Thread.sleep(3000);
        		}catch(Exception e){}
        		LedCon(0xff);
        		
        		// 원래 실행하던 어플을 종료하고,
        		moveTaskToBack(true);
			    finish();
        		// 블루투스 어플 실행 후 데이터 전송
        		startActivity(load_bluetooth);
        	}
        	});
        
        mButton2.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        	
        		moveTaskToBack(true);
			    finish();
        	
        	}
        	});
        
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action))
        {
        	NdefMessage[] messages = getNdefMessages(getIntent());
        	byte[] payload = messages[0].getRecords()[0].getPayload();
        	
        	//mText.setText(new String(payload));
        	
        	mText.setText("결제가 완료 되었습니다 ");
        	mText.setTextSize(20);
        	
        	//ByteBuffer wrapped1 = ByteBuffer.wrap(payload);
        	//int num1 = wrapped1.getInt();
        	
        	
        	//ByteBuffer dbuf = ByteBuffer.allocate(payload.length + 1);
        	//dbuf.putInt(num1);
        	//byte[] payload2 = dbuf.array();
        	
        	//num1 = num1 - (82524164*2); // 825241647
        	
        	//String temp1 = Integer.toString(num1);

        	//int num2 = Integer.parseInt(temp1);
        	
        	//num2 = num2 + 1;
        	
        	//String temp2 = Integer.toString(num2);
        	
        	//mText.setText(temp1);
        	//mText.setText(temp2);
        	//mText.setText(Integer.toString(num1));
        	//mText.setText(new String(payload));
        	//mText.setText(new String(payload2));
        	//mText.setText(new Integer(temp1));
        }
        else
        {
        	mText.setText("Scan a tag");
        }
        
        
        /* try
    	{
    		Thread.sleep(3000);
    	}
    	catch(Exception e)
    	{}*/
        
        // 원래 실행하던 어플을 종료하고,
		moveTaskToBack(true);
	    finish();
	    
    	startActivity(loadpattern);					// nfc로 읽으면, 패턴창이 실행됨
    	
    }
    NdefMessage[] getNdefMessages(Intent intent)
    {
    	NdefMessage[] msgs = null;
    	String action = intent.getAction();
    	
    	if(NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action))
    	{
    		Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
    		if(rawMsgs != null)
    		{
    			msgs = new NdefMessage[rawMsgs.length];
    			
    			for(int i=0;i<rawMsgs.length;i++)
    			{
    				msgs[i] = (NdefMessage) rawMsgs[i];
    			}
    		}
    	}
    	return msgs;
    }    
}
