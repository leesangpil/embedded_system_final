package net.hybus.success;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class SuccessActivity extends Activity {

	
	private Button mButton1;
	private Button mButton2;
	
	
	static {
		System.loadLibrary("ledjni");
	}
	///////////////여기 밑에 1중
	public native int LedCon(int led);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.success_main);
		
		mButton1 = (Button) findViewById(R.id.button1);
        mButton2 = (Button) findViewById(R.id.button2);
        
        final Intent load_bluetooth = this.getPackageManager().getLaunchIntentForPackage("com.example.android.BluetoothChat");
        
        mButton1.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        	
        		LedCon(0x00);
        		try{
        		Thread.sleep(3000);
        		}catch(Exception e){}
        		LedCon(0xff);
        		
        		// 원래 실행하던 어플을 종료하고,
        		moveTaskToBack(true);
			    finish();
        		// 블루투스 어플 실행 후 데이터 전송
        		startActivity(load_bluetooth);
        	}
        	});
        
        mButton2.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        	
        		moveTaskToBack(true);
			    finish();
        	
        	}
        	});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.success, menu);
		return true;
	}

}
